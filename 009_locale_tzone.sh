#!/bin/bash

. ./errors.sh
. ./config.sh

cat << 'EOF' > /mnt/gentoo/etc/locale.gen
en_US.UTF-8 UTF-8
EOF

chroot /mnt/gentoo locale-gen
LOCALE_ID=$(chroot /mnt/gentoo eselect locale list | grep 'en_US.utf8' | awk '{ print $1 }' | grep -oE '[0-9]+')
chroot /mnt/gentoo eselect locale set ${LOCALE_ID}
chroot /mnt/gentoo env-update

echo "Europe/Warsaw" > /mnt/gentoo/etc/timezone
chroot /mnt/gentoo emerge --config sys-libs/timezone-data
#!/bin/bash

. ./errors.sh
. ./config.sh

chroot /mnt/gentoo emerge-webrsync
chroot /mnt/gentoo emerge --sync

chroot /mnt/gentoo eselect news read all --quiet
chroot /mnt/gentoo eselect news purge

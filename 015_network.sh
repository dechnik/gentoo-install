#!/bin/bash

. ./errors.sh
. ./config.sh

chroot /mnt/gentoo emerge net-misc/dhcp net-misc/iputils sys-apps/iproute2

echo "gomen" > /mnt/gentoo/etc/hostname
echo 'hostname="gomen"' > /mnt/gentoo/etc/conf.d/hostname

cat << EOF > /mnt/gentoo/etc/hosts
# /etc/hosts

127.0.0.1   gomen localhost localhost4
EOF

chroot /mnt/gentoo rc-update add hostname boot

chroot /mnt/gentoo emerge net-misc/networkmanager net-dns/dnsmasq

cat << EOF > /mnt/gentoo/etc/NetworkManager/NetworkManager.conf
[main]
dns=dnsmasq
EOF

chroot /mnt/gentoo rc-update add NetworkManager default
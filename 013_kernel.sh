#!/bin/bash

. ./errors.sh
. ./config.sh

mkdir -p /mnt/gentoo/etc/portage/package.accept_keywords
echo 'sys-kernel/gentoo-sources ~amd64' > /mnt/gentoo/etc/portage/package.accept_keywords/kernel
chroot /mnt/gentoo emerge sys-kernel/gentoo-sources

cp ./configs/desktop.config /mnt/gentoo/usr/src/linux/.config

chroot /mnt/gentoo /bin/bash -c "cd /usr/src/linux; make olddefconfig"
chroot /mnt/gentoo /bin/bash -c "cd /usr/src/linux; make --jobs $($nproc) --load-average $(nproc)"
chroot /mnt/gentoo /bin/bash -c "cd /usr/src/linux; make modules_install"
chroot /mnt/gentoo /bin/bash -c "cd /usr/src/linux; make bzImage"
chroot /mnt/gentoo /bin/bash -c "cp /usr/src/linux/arch/x86/boot/bzImage /boot/vmlinuz-current"

cat << EOF > /mnt/gentoo/etc/dracut.conf.d/00_base_system.conf
# /etc/dracut.conf.d/00_base_system.conf

dracutmodules="base bash caps crypt dm i18n kernel-modules lvm rootfs-block terminfo udev-rules usrmount"

#add_dracutmodules+="plymouth"

compress="lz4"
hostonly="no"
reproducible="yes"

do_strip="yes"
do_prelink="no"

persistent_policy="by-uuid"
EOF

if [ "${CRYPT}" = "yes" ]; then
    chroot /mnt/gentoo /bin/bash -c 'dracut --kver $(ls /lib/modules/ | sort -rn | head -n 1) -a crypt -f /boot/initramfs-current.img'
else
    chroot /mnt/gentoo /bin/bash -c 'dracut --kver $(ls /lib/modules/ | sort -rn | head -n 1) -f /boot/initramfs-current.img'
fi
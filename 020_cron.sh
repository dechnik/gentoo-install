#!/bin/bash

. ./errors.sh
. ./config.sh

echo 'sys-process/cronie anacron' > /mnt/gentoo/etc/portage/package.use/cron

chroot /mnt/gentoo emerge sys-process/cronie
chroot /mnt/gentoo rc-update add cronie default
#!/bin/bash

. ./errors.sh
. ./config.sh

chroot /mnt/gentoo emerge app-portage/cpuid2cpuflags
chroot /mnt/gentoo echo "*/* $(chroot /mnt/gentoo cpuid2cpuflags)" > /mnt/gentoo/etc/portage/package.use/00cpuflags

chroot /mnt/gentoo emerge --update --newuse --deep @world
chroot /mnt/gentoo emerge @preserved-rebuild
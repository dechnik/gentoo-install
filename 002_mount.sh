#!/bin/bash

. ./errors.sh
. ./config.sh

/etc/init.d/lvmetad start

if [ "${CRYPT}" = "yes" ]; then
  if [ ! -b /dev/mapper/crypt ]; then
      cryptsetup luksOpen --allow-discards ${DISK}3 crypt
  fi

  sleep 1

  pvscan
  lvscan
fi

if lvscan | grep system | grep -q inactive &> /dev/null; then
  vgchange --activate y system
fi

mkdir -p /mnt/gentoo
mount -o defaults,discard,noatime /dev/mapper/system-root /mnt/gentoo
swapon /dev/mapper/system-swap
mkdir -p /mnt/gentoo/boot
mount ${DISK}2 /mnt/gentoo/boot

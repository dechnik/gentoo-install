#!/bin/bash

. ./errors.sh
. ./config.sh

chroot /mnt/gentoo eselect profile set default/linux/amd64/17.0
chroot /mnt/gentoo emerge --update --newuse --deep @world
chroot /mnt/gentoo emerge @preserved-rebuild

chroot /mnt/gentoo emerge sys-kernel/linux-firmware
#!/bin/bash

. ./errors.sh
. ./config.sh

cat << EOF > /mnt/gentoo/etc/environment
# /etc/environment

# This file is parsed by the pam_env module
# Syntax: simple "KEY=VAL" pairs on separate lines
EOF

eval `blkid -o export ${DISK}2`
cat << EOF > /mnt/gentoo/etc/fstab
# /etc/fstab

UUID="${UUID}"          /boot   vfat  defaults,noatime,noexec   0 2
/dev/mapper/system-swap   none    swap  defaults                  0 0
/dev/mapper/system-root   /       ext4   defaults,noatime          0 1

tmpfs                     /tmp   tmpfs  rw,nosuid,nodev,noatime   0 0
EOF

if [ "${CRYPT}" = "yes" ]; then
    chroot /mnt/gentoo emerge sys-fs/cryptsetup
    echo "crypt       ${DISK}3    luks" > /mnt/gentoo/etc/crypttab
fi

cat << EOF > /mnt/gentoo/etc/env.d/00prioritypath
PATH="/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin"
ROOTPATH="/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/sbin:/usr/local/bin"
EOF

chroot /mnt/gentoo /usr/sbin/env-update

cat << EOF > /mnt/gentoo/etc/profile.d/editor_preference.sh
# /etc/profile.d/editor_preference.sh
export EDITOR="/usr/bin/vim"
EOF
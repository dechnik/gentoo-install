#!/bin/bash

. ./errors.sh
. ./config.sh

chroot /mnt/gentoo emerge \
    sys-apps/dmidecode \
    app-arch/lz4 \
    net-misc/curl \
    sys-apps/lm_sensors \
    sys-apps/usbutils \
    sys-apps/pciutils \
    sys-block/parted \
    sys-fs/cryptsetup \
    sys-fs/dosfstools \
    sys-fs/lvm2 \
    sys-fs/xfsprogs \
    app-editors/vim \
    sys-kernel/dracut

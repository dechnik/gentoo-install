#!/bin/bash

. ./errors.sh
. ./config.sh

cat << 'EOF' > /mnt/gentoo/etc/portage/package.use/ssh
net-misc/openssh ldns
EOF

chroot /mnt/gentoo emerge net-misc/openssh

cat << 'EOF' > /mnt/gentoo/etc/ssh/ssh_config
# /etc/ssh/ssh_config

CheckHostIP yes
HashKnownHosts yes
IdentitiesOnly yes

PreferredAuthentications publickey,keyboard-interactive,password

# For Kerberos
#PreferredAuthentications gssapi-keyex,gssapi-with-mic,publickey,keyboard-interactive

#GSSAPIAuthentication yes
#GSSAPIDelegateCredentials yes
#GSSAPITrustDns yes

#CanonicalizeHostname yes
#CanonicalDomains prd.aus.devtty.net devtty.org
#CanonicalizePermittedCNAMEs *.prd.aus.devtty.org:*.devtty.org

#Host *.devtty.org
#  VerifyHostKeyDNS yes
EOF


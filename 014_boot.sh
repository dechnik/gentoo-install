#!/bin/bash

. ./errors.sh
. ./config.sh

mkdir -p /mnt/gentoo/etc/portage/package.accept_keywords

echo 'app-crypt/efitools ~amd64' > /mnt/gentoo/etc/portage/package.accept_keywords/efi
chroot /mnt/gentoo emerge app-crypt/efitools sys-boot/efibootmgr sys-boot/grub

RUUID=$(chroot /mnt/gentoo blkid /dev/mapper/system-root | sed -n 's/.* UUID=\"\([^\"]*\)\".*/\1/p')
LUUID=$(chroot /mnt/gentoo blkid ${DISK}3 | sed -n 's/.* UUID=\"\([^\"]*\)\".*/\1/p')
cat << EOF > /mnt/gentoo/etc/default/grub
# GPD also wants "i915.fastboot=1 fbcon=rotate:1"
# Plymouth wants "quiet splash"
# GRUB_CMDLINE_LINUX_DEFAULT="lvm net.ifnames=0 root=UUID=${RUUID}  rd.luks.uuid=${LUUID}"

GRUB_DEFAULT=1
GRUB_TIMEOUT=1

GRUB_DISABLE_LINUX_UUID=false
GRUB_DISABLE_RECOVERY=true
GRUB_DISABLE_SUBMENU=y
EOF

if [ "${CRYPT}" = "yes" ]; then
    echo 'GRUB_CMDLINE_LINUX_DEFAULT="lvm net.ifnames=0 root=UUID=${RUUID} rd.luks.uuid=${LUUID}"' >> /mnt/gentoo/etc/default/grub
else
    echo 'GRUB_CMDLINE_LINUX_DEFAULT="lvm net.ifnames=0 root=UUID=${RUUID}"' >> /mnt/gentoo/etc/default/grub
fi

chroot /mnt/gentoo grub-install --target=x86_64-efi --efi-directory=/boot --removable
chroot /mnt/gentoo grub-mkconfig -o /boot/grub/grub.cfg

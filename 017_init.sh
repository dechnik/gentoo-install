#!/bin/bash

. ./errors.sh
. ./config.sh

sed -i '/ctrlaltdel/d' /mnt/gentoo/etc/inittab

cat << 'EOF' > /mnt/gentoo/etc/rc.conf
# /etc/rc.conf

rc_controller_cgroups="YES"
rc_interactive="NO"
rc_shell="/sbin/sulogin"
rc_tty_number="9"

rc_logger="YES"
rc_parallel="YES"

unicode="YES"
EOF
#!/bin/bash

. ./errors.sh
. ./config.sh

cat << EOF > /mnt/gentoo/etc/portage/make.conf
COMMON_FLAGS="-march=native -O2 -pipe"
CFLAGS="\${COMMON_FLAGS}"
CXXFLAGS="\${COMMON_FLAGS}"
FCFLAGS="\${COMMON_FLAGS}"
FFLAGS="\${COMMON_FLAGS}"
CHOST="x86_64-pc-linux-gnu"
GRUB_PLATFORMS="efi-64 pc"

EMERGE_DEFAULT_OPTS="--jobs $(nproc) --load-average $(nproc) --verbose --tree --keep-going"

PORTDIR="/usr/portage"
DISTDIR="/usr/portage/distfiles"
PKGDIR="/usr/portage/packages"

LC_MESSAGES=C
L10N="en"
LINGUAS="en"

USE="acpi avahi caps crypt cryptsetup curl dbus hddtemp libnotify lm_sensors mmx networkmanager pam posix python readline readline sse sse2 sse3 ssse3 unicode usb -systemd"
EOF

mirrorselect -o -s 3 -q -D -H -c 'Poland' >> /mnt/gentoo/etc/portage/make.conf
mkdir -p /mnt/gentoo/etc/portage/package.use
echo 'app-shells/bash plugins' > /mnt/gentoo/etc/portage/package.use/bash
echo 'sys-apps/busybox -static' > /mnt/gentoo/etc/portage/package.use/busybox
mkdir -p /mnt/gentoo/etc/portage/repos.conf
cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf
sed 's/rsync.gentoo/rsync1.pl.gentoo/g' -i /mnt/gentoo/etc/portage/repos.conf/gentoo.conf
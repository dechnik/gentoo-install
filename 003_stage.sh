#!/bin/bash

. ./errors.sh
. ./config.sh

wget http://ftp.vectranet.pl/gentoo/releases/amd64/autobuilds/20190303T214502Z/stage3-amd64-20190303T214502Z.tar.xz
tar xpvf stage3-*.tar.xz --xattrs-include='*.*' --numeric-owner -C /mnt/gentoo
rm -f stage3-*.tar.xz
